\section{Proposed Approach}
\label{sec:method}

%As shown in Fig.~\ref{fig:overview}, our proposed framework consists of four modules. The reconstructor $R$ takes an input image and predicts its 3DMM coefficients. The generator $G$ takes the input image and the estimated 3DMM coefficients to produce a frontal face of the same subject. The discriminator $D$ distinguish between the generated and the ground truth frontal faces. The recognizer $C$ regularize the identity classification of the input and the fake images. 

The mainstay of FF-GAN is a generative adversarial network that consists of a generator $G$ and a discriminator $D$. $G$ takes a non-frontal face as input to generate a frontal output, while $D$ attempts to classify it as a real frontal image or a generated one. Additionally, we include a face recognition engine $C$ that regularizes the generator output to preserve identity features. A key component is a deep 3DMM model $R$ that provides shape and appearance priors to the GAN that play a crucial role in alleviating the difficulty of large pose face frontalization. Figure \ref{fig:overview} summarizes our framework.

%As shown in Fig.~\ref{fig:overview}, the mainstay of our framework is a generative adversarial network consists of a generator $G$ and a discriminator $D$. To alleviate the difficulty of frontalization while preserving high frequency appearance, we propose a deep 3DMM, denoted as model $R$, on top of the GAN to provide additional shape and appearance information. The explicitly injected prior largely reduce the training complexity and show better performance. Moreover, to preserve the identity during generation, similar to~\cite{tran2017}, we propose a recognition engine, model $C$, to regularize the identity feature extracted on the GAN's output.

Let $\mathbb{D} =\{ {{\bf{x}}_i, {\bf{x}}_i^g, {\bf{p}}_i^g}, y_i\}_{i=1}^{N}$ be the training set with $N$ samples, with each sample consisting of an input image ${\bf{x}}_i$ with arbitrary pose, a corresponding ground truth frontal face ${\bf{x}}_i^g$, the ground truth 3DMM coefficients ${\bf{p}}_i^g$ and the identity label $y_i$. We henceforth omit the sample index $i$ for clarity. 


\subsection{Reconstruction Module}
\label{sec:reconstruction}
Frontalization from extreme pose variations is a challenging problem. While a purely data-driven approach might be possible given sufficient data and an appropriate training regimen, however it is non-trivial. Therefore, we propose to impose a prior on the generation process, in the form of a 3D Morphable Model (3DMM) \cite{3dmm}. This reduces the training complexity and leads to better empirical performance with limited data.

Recall that 3DMM represents faces in the PCA space:
\vspace{-2mm}
\begin{equation}
\begin{aligned}
{\bf{S}} {} & = {\bf{ \bar{S}}} + {\bf{A}}_{id}{\bf{\alpha}}_{id} + {\bf{A}}_{exp}{\bf{\alpha}}_{exp}, \\
{\bf{T}} {} & = {\bf{\bar{T}}} + {\bf{A}}_{tex}{\bf{\alpha}}_{tex},
\label{eq:3d}
\end{aligned}
\end{equation}
where ${\bf{S}}$ are the 3D shape coordinates computed as the linear combination of the mean shape $\bf{\bar{S}}$, the shape basis ${\bf{A}}_{id}$ and the expression basis ${\bf{A}}_{exp}$, while ${\bf{T}}$ is the texture that is the linear combination of the mean texture $\bf{\bar{T}}$ and the texture basis ${\bf{A}}_{tex}$. 
The coefficients $\{ {\bf{\alpha}}_{id}, {\bf{\alpha}}_{exp}, {\bf{\alpha}}_{tex} \}$ defines a unique 3D face. 
%The 3DMM fitting process is to estimate the coefficients $\{ {\bf{\alpha}}_{id}, {\bf{\alpha}}_{exp}, {\bf{\alpha}}_{tex} \}$ that defines a unique 3D face.

Previous work~\cite{jourabloo2015pose, zhu2016face} applies 3DMM for face alignment where a weak perspective projection model is used to project the 3D shape into 2D space. Similar to \cite{jourabloo2015pose}, we calculate a projection matrix ${\bf{m}}\in\mathbb{R}^{2\times4}$ based on pitch, yaw, roll, scale and 2D translations to represent the pose of an input face image. Let ${\bf{p}} = \{ {\bf{m}}, {\bf{\alpha}}_{id}, {\bf{\alpha}}_{exp}, {\bf{\alpha}}_{tex}\}$ denotes the 3DMM coefficients. The target of the reconstruction module $R$ is to estimate ${\bf{p}} = R({\bf{x}})$, given an input image ${\bf{x}}$. Since the intent is for $R$ to also be trainable with the rest of the framework, we use a CNN model based on CASIA-Net~\cite{yi2014learning} for this regression task. We apply $z$-score normalization to each dimension of the parameters before training. A weighted parameter distance cost similar to \cite{zhu2016face} is used:
\begin{equation}
\min_{\bf{p}}L_R = ({\bf{p}} - {\bf{p}}^g)^\top {\bf{W}} ({\bf{p}} - {\bf{p}}^g),
\label{eq:objR}
\end{equation}
where ${\bf{W}}$ is the importance matrix whose diagonal is the weight of each parameter.



\subsection{Generation Module}
\label{sec:generation}

%Our generator $G$ takes both the input image and the estimated 3DMM coefficients and aims to generate a frontal face of the same subject. The motivation is to leverage 3DMM for face frontalization. Since 3DMM is limited to a linear space, the coefficients alone are not sufficient to decode a frontal face image that maintains the high-frequency identity information. The input image is used to compensate the loss of the discriminative identity features in $R$. 

The pose estimate obtained from 3DMM is quite accurate, however, frontalization using it leads to loss of high frequency details present in the original image. This is understandable, since a low-dimensional PCA representation can preserve most of the energy in lower frequency components. Thus, we use a generative model that relies on 3DMM coefficients ${\bf{p}}$ and the input image ${\bf{x}}$ to recover a frontal face that preserves both the low and high frequency components.

%Direct face frontalization with the 3DMM model suffers from the low quality of the low rank reconstruction, which lacks the high frequency appearance from the original image. The generative model shows strong capability in mapping source data distribution to the target one, and has revealed better generation quality other than the one from least square loss~\cite{tran2017}.
%Given that the coefficients $\bf{p}$ from the 3DMM reconstruction model is low dimensional and may lose the high frequency information, we seek the high frequency information from the original input $\bf{x}$. Combining the 3DMM coefficients with the input image, we obtain the low frequency frontal face appearance from the 3DMM coefficients and retrieve the high frequency appearance by learning the implicit correspondence from pose-variant input to the frontalized output. 

In Figure \ref{fig:overview}, features from the two inputs to the generator $G$ are fused through an encoder-decoder network to synthesize a frontal face ${\bf{x}}^f = G({\bf{x}}, {\bf{p}})$. To penalize the output from ground truth ${\bf{x}}^g$, the straight-forward objective is the reconstruction loss that aims at reconstructing the ground truth with minimal error:
\vspace{-2mm}
\begin{equation}
L_{G_{rec}} = \| G({\bf{x}}, {\bf{p}}) - {\bf{x}}^g\|_1.
\label{eqn:grec}
\end{equation}
Since an $L_2$ loss empirically leads to blurry output, we use an $L_1$ loss to better preserve high frequency signals. At the beginning of training, the reconstruction loss harms the overall process since the generation is far from frontalized, so the reconstruction loss operates on a poor set of correspondences. Thus, the weight for reconstruction loss should be set in accordance to the training stage. The details of tuning the weight are discussed in Section~\ref{sec:implementation}.

To reduce block artifacts, we use a spatial total variation loss to encourage smoothness in the generator output:
%Given the output from $G$ as ${\bf{x}}^f$, it is defined in Equation~\ref{eqn:gtv}:
\vspace{-2mm}
\begin{equation}
L_{G_{tv}} = \frac{1}{|\Omega|}\int_{\Omega}|\nabla G({\bf{x}}, {\bf{p}})|du,
\label{eqn:gtv}
\end{equation}
where $|\nabla G|$ is the image gradient, $u\in\mathbb{R}^{2}$ is the two dimensional coordinate increment, $\Omega$ is the image region and $|\Omega|$ is the area normalization factor.

Based on the observation that human faces share self-similarity across left and right halves, we explicitly impose a symmetry loss. We recover a frontalized 2D projected silhouette, $\mathcal{M}$, from the frontalized 3DMM model indicating the visible parts of the face. The mask $\mathcal{M}$ is binary, with nonzero values indicating visible regions and zero otherwise. By horizontally flipping the face, we achieve another mask $\mathcal{M}_{flip}$. We demand that generated frontal images for the original input image and its flipped version should be similar within their respective masks:
\begin{align}
&L_{G_{sym}} = \|\mathcal{M}\odot G({\bf{x}},{\bf{p}}) - \mathcal{M}\odot G({\bf{x}}_{flip}, {\bf{p}}_{flip})\|_{2} \notag \\
&+ \|{\mathcal{M}}_{flip}\odot G({\bf{x}},{\bf{p}}) - {\mathcal{M}}_{flip}\odot G({\bf{x}}_{flip}, {\bf{p}}_{flip})\|_{2}.
\label{eqn:gtv}
\end{align}
Here, ${\bf{x}}_{flip}$ is the horizontal flipped image for input $\bf{x}$, ${\bf{p}}_{flip}$ are the 3DMM coefficients for ${\bf{x}}_{flip}$ and $\odot$ denotes element-wise multiplication. Note that the role of the mask is to focus only the face portions of the image, rather than background.

%We emphasize on the mask is because those invisible parts during rotation is not confident to contribute to the penalty, whereas we only focus on the visible parts for both original image and flipped image. We claim that the same subject with both original and flipped input should achieve the same frontal output.


\subsection{Discrimination Module}
\label{sec:discrimination}

Generative Adversarial Networks~\cite{goodfellow2014generative}, formulated as a two-player game between a generator and a discriminator, have been widely used for image generation. In this work, the generator $G$ synthesizes a frontal face image ${\bf{x}}^f$ and the discriminator $D$ distinguishes between the generated face from frontal faces ${\bf{x}}^g$. Note that in a conventional GAN, all images used for training are considered as real samples. However, we limit ``real'' faces to frontal views only. Thus, $G$ must generate both realistic and frontal face images. 

%The objective for training $D$ is to minimize the classification loss between the real image ${\bf{x}}^g$ and the fake image ${\bf{x}}^f$ that is generated by $G$. 
Our $D$ consists of five convolutional layers and one linear layer that generates a 2D vector with each dimension representing the probability of the input to be real or generated. During training, $D$ is updated with two batches of samples in each iteration. The following objective is maximized: 
\vspace{-2mm}
\begin{equation}
\min_{D} L_D = \mathbb{E}_{{\bf{x}}^g\in \mathcal{R}}\log(D({\bf{x}}^g)) + \mathbb{E}_{{\bf{x}}\in \mathcal{K}}\log(1-D(G({\bf{x}}, {\bf{p}})))
\label{eq:objD}
\end{equation}
where $\mathcal{R}$ and $\mathcal{K}$ are the real and generated image sets respectively.

On the other hand, $G$ aims to fool $D$ to classify the generated image to be real with the following loss:
\vspace{-2mm}
\begin{align}
L_{G_{gan}} = \mathbb{E}_{{\bf{x}}\in \mathcal{K}}\log(D(G({\bf{x}},{\bf{p}})))
\end{align}

The competition between $G$ and $D$ improves both modules. In the early stages when face images are not fully frontalized, $D$ focuses on the pose of the face to make the real or generated decision, which in turn helps $G$ to generate a frontal face. In the later stages when face images are frontalized, $D$ focuses on subtle details of frontal faces, which guides $G$ to generate a realistic frontal face that is difficult to achieve with the supervisions of \eqref{eqn:grec} and \eqref{eqn:gtv} alone. 

\subsection{Recognition Module}
\label{sec:recognition}
A key challenge in large-pose face frontalization is to preserve the original identity. This is a difficult task due to self-occlusion in profile faces. The discriminator above can only determine whether the generated image is realistic and in frontal pose, but does not consider whether identity of the input image is retained. Although we have L1, total variation and masked symmetry losses for face generation, they treat each pixel equally that result in the loss of discriminative power for identity features. Therefore, we use a recognition module $C$ to impart correct identity to the generated images. 

We use a CASIA-Net structure for the recognition engine $C$, with a cross-entropy loss for training $C$ to classify image ${\bf{x}}$ with the ground truth identity $y$:
\vspace{-2mm}
\begin{equation}
\small
\min_{C}L_C = \sum_{j}\left[-y_j\log(C_j({\bf{x}})) - (1 - y_j) \log(1-C_j({\bf{x}}))\right]
\label{eq:objC}
\end{equation}
Where $j$ is the index of the identity classes. Now, our generator $G$ must also fool $C$ to classify the generated image to have the same identity as the input image. If the identity label of the input image is not available, we regularize the extracted identity features ${\bf{h}}^f$ of the generated image to be similar to those of the input image, denoted as ${\bf{h}}$. During training, $C$ is updated with real input images to retain discriminative power and the loss from the generated images is back-propagated to update the generator $G$:
\vspace{-2mm}
\begin{equation}
\small
L_{G_{id}} =
\begin{cases}
-\log(C(G({\bf{x}}, {\bf{p}}))), & \exists y \\
\| {\bf{h}}^f - {\bf{h}}\|_2^2, & \nexists y,
\end{cases}
\vspace{-2mm}
\end{equation}

To summarize the framework, the reconstruction module $R$ provides guidance to the frontalization through \eqref{eq:objR}, the discriminator does so through \eqref{eq:objD} and the recognition engine through \eqref{eq:objC}. Thus, the generator combines all these sources of information to optimize an overall objective function:
\vspace{-2mm}
\begin{align}
&\min_{G} L_{G}=\lambda_{rec}L_{G_{rec}} + \lambda_{tv}L_{G_{tv}} \notag \\
&+ \lambda_{sym}L_{G_{sym}} + \lambda_{gan}L_{G_{gan}} + \lambda_{id}L_{G_{id}}.
\label{eq:objall}
\end{align}
The weights above are discussed in Section~\ref{sec:implementation} to illustrate how each component contributes to joint optimization of $G$.









