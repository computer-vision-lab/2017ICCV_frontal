
sub = [256,263,267,285,289,291];

for i = 1 : length(sub)
    img = imread(['subject_', num2str(sub(i)), '.jpg']);
    img1 = img(1:100, 101:200, :);
    img2 = img(101:200, 101:200, :);
    img3 = img(1:100,601:700,:);
    img4 = img(101:200, 601:700, :);
    imwrite(img1, ['subject_', num2str(sub(i)), '_1.jpg']);
    imwrite(img2, ['subject_', num2str(sub(i)), '_1_fake.jpg']);
    imwrite(img3, ['subject_', num2str(sub(i)), '_2.jpg']);
    imwrite(img4, ['subject_', num2str(sub(i)), '_2_fake.jpg']);
end
