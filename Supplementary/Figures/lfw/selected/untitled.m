 
% imgIdx1 = [6,8,10,12,14,24,32,36,37,44,64,65,73,98,101,109,112,115,116,133];
% idxs1 = [2,2,2,5,7,3,1,1,3,1,3,8,4,6,1,2,4,2,8,3];
% imgIdx2 = [23,35,118,134,135,135];
% idxs2 = [3,5,6,2,1,6];
imgIdx1 = [3,3,12,17,19,22,27,31,39,50,50,61,70,71,101,127,127,135,146];
idxs1 = [1,8,8,6,8,6,8,6,1,1,3,4,5,1,3,4,7,7,6];

for im = 1 : length(imgIdx1)
    img = imread(['selected_2/ref1_', num2str(imgIdx1(im)), '.jpg']);
    
    id = idxs1(im);
    img1 = img(1:100, (id-1)*100+1:id*100,:);
    img2 = img(101:200, (id-1)*100+1:id*100,:);
    img3 = img(201:300, (id-1)*100+1:id*100,:);
    img4 = img(301:400, (id-1)*100+1:id*100,:);
    
    cnt = (imgIdx1(im)-1)*8 + id;
    imwrite(img1, ['crop_2/ref1_', num2str(cnt), '.jpg'])
    imwrite(img2, ['crop_2/ref1_', num2str(cnt), '_3d.jpg'])
    imwrite(img3, ['crop_2/ref1_', num2str(cnt), '_hpen.jpg'])
    imwrite(img4, ['crop_2/ref1_', num2str(cnt), '_ffgan.jpg'])
end

