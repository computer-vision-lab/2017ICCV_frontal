
images = dir('new/subject_*.jpg');
for im = 1 : length(images)
    img = imread(['new/',images(im).name]);
    input = img(:,1:100,:);
    gt = img(:,end-99:end,:);
    imwrite(input, ['subfig/', images(im).name(1:end-4), '_input.jpg']);
    imwrite(gt, ['subfig/', images(im).name(1:end-4), '_gt.jpg']);
    for i = 1 : 10
        img1 = img(:,i*100+26:i*100+125,:);
        imwrite(img1, ['subfig/', images(im).name(1:end-4), '_', num2str(i), '.jpg'])
    end
    disp(im)
end
